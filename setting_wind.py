#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication, QMainWindow, QAction, qApp, QComboBox)
from PyQt5.QtGui import QPixmap, QColor, QIcon, QFont
from PyQt5.QtCore import QCoreApplication, QTimer, QSize, QRect

import PyQt5.QtCore as QtCore


from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusSerialClient


import serial

# --->>> на дисплей
# <<<--- на контроллер






def serial_ports():
    """ Lists serial port names
        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result




class Window_com_port(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi()
        self.Port.addItems(serial_ports())
        self.realport = None
        # self.ConnectButton.clicked.connect(self.connect_modbus)

        self.connect_status = 0
        self.client = 0

    def setupUi(self):

        self.Port = QComboBox(self)
        self.Port.setGeometry(QRect(10, 30, 381, 31))
        self.Port.setObjectName("Port")

        self.label = QLabel(self)
        self.label.move(170, 0)
        font = QFont()
        font.setPointSize(8)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label.setText("Порт")

        self.ConnectButton = QPushButton(self)
        self.ConnectButton.setText("Подключиться")
        self.ConnectButton.move(10, 70)
        self.ConnectButton.clicked.connect(self.connect_modbus)

        self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 400, 300)
        self.setWindowTitle("Настройка СOM порта")

    def connect_modbus(self):
        try:
            self.client = ModbusSerialClient(method="rtu", port=self.Port.currentText(), stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
            connection = self.client.connect()
            # self.ConnectButton.setStyleSheet("background-color: green")
            self.ConnectButton.setText('Подключено')
            self.ConnectButton.setEnabled(False)
            # добавить отдельноеполе на параметр подключения
            self.connect_status = self.client.connect()
            print("Connect_status", self.connect_status)

        except Exception as e:
            print(e)


class Window_setting(QMainWindow):                           # <===
    def __init__(self):
        super().__init__()

        # Настройки
        # <<<---
        # время вкл выкидного транспортера
        self.time_on_conveyor_1 = 0
        # время выкл выкидного транспортера
        self.time_off_conveyor_1 = 0
        # время выключения молотковой дробилки
        self.time_off_hammer = 0
        # предпусковой звонок
        self.pre_launch = 0
        # время прямого вращения
        self.time_direct_rotation = 0
        # время обратного вражения
        self.time_reverse_rotation = 0
        # пауза на остановку
        self.pause_to_stop = 0


        self.time_on_conveyor_1_status = 0
        self.time_off_conveyor_1_status = 0
        self.time_off_hammer_status = 0
        self.pre_launch_status = 0
        self.time_direct_rotation_status = 0
        self.pause_to_stop_status = 0

        self.status_but = 0




        self.qbtn_load = QPushButton('Установить настройки', self)
        self.qbtn_load.clicked.connect(self.load_setting_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_load.adjustSize()
        self.qbtn_load.move(50, 400)

        self.qbtn_2_m = QPushButton('', self)
        self.qbtn_2_m.clicked.connect(self.load_kay_bord)
        self.qbtn_2_m.setIcon(QIcon('img/kay_bord.png'))
        self.qbtn_2_m.setToolTip('Включить клавиатуру')
        self.qbtn_2_m.setIconSize(QSize(45, 45))
        self.qbtn_2_m.resize(50, 50)
        self.qbtn_2_m.move(500, 500)

        self.lbl_to = QLabel(self)
        self.lbl_to.setText("Задать настройки на ПЛК")
        self.lbl_to.adjustSize()
        self.lbl_to.move(50, 10)

        self.lbl_to_1 = QLabel(self)
        self.lbl_to_1.setText("Время вкл. выкидного транспортера")
        self.lbl_to_1.adjustSize()
        self.lbl_to_1.move(50, 50)

        self.qle_to_1 = QLineEdit(self)
        self.qle_to_1.resize(200, 25)
        self.qle_to_1.move(50, 70)
        self.qle_to_1.textChanged[str].connect(self.onChanged_1)

        self.lbl_to_2 = QLabel(self)
        self.lbl_to_2.setText("Время выкл. выкидного транспортера")
        self.lbl_to_2.adjustSize()
        self.lbl_to_2.move(50, 100)

        self.qle_to_2 = QLineEdit(self)
        self.qle_to_2.resize(200, 25)
        self.qle_to_2.move(50, 120)
        self.qle_to_2.textChanged[str].connect(self.onChanged_2)

        self.lbl_to_3 = QLabel(self)
        self.lbl_to_3.setText("Время выкл. молотковой дробилки")
        self.lbl_to_3.adjustSize()
        self.lbl_to_3.move(50, 150)

        self.qle_to_3 = QLineEdit(self)
        self.qle_to_3.resize(200, 25)
        self.qle_to_3.move(50, 170)
        self.qle_to_3.textChanged[str].connect(self.onChanged_3)

        self.lbl_to_4 = QLabel(self)
        self.lbl_to_4.setText("Предпусковой звонок (вкл/выкл)")
        self.lbl_to_4.adjustSize()
        self.lbl_to_4.move(50, 200)

        self.qle_to_4 = QLineEdit(self)
        self.qle_to_4.resize(200, 25)
        self.qle_to_4.move(50, 220)
        self.qle_to_4.textChanged[str].connect(self.onChanged_4)

        self.lbl_to_5 = QLabel(self)
        self.lbl_to_5.setText("Время прямого вращения")
        self.lbl_to_5.adjustSize()
        self.lbl_to_5.move(50, 250)

        self.qle_to_5 = QLineEdit(self)
        self.qle_to_5.resize(200, 25)
        self.qle_to_5.move(50, 270)
        self.qle_to_5.textChanged[str].connect(self.onChanged_5)

        self.lbl_to_6 = QLabel(self)
        self.lbl_to_6.setText("Время обратного вращения")
        self.lbl_to_6.adjustSize()
        self.lbl_to_6.move(50, 300)

        self.qle_to_6 = QLineEdit(self)
        self.qle_to_6.resize(200, 25)
        self.qle_to_6.move(50, 320)
        self.qle_to_6.textChanged[str].connect(self.onChanged_6)


        self.lbl_to_7 = QLabel(self)
        self.lbl_to_7.setText("Пауза на остановку")
        self.lbl_to_7.adjustSize()
        self.lbl_to_7.move(50, 350)

        self.qle_to_7 = QLineEdit(self)
        self.qle_to_7.resize(200, 25)
        self.qle_to_7.move(50, 370)
        self.qle_to_7.textChanged[str].connect(self.onChanged_7)


        self.lbl_from = QLabel(self)
        self.lbl_from.setText("Текущие настройки на ПЛК")
        self.lbl_from.adjustSize()
        self.lbl_from.move(350, 10)

        self.lbl_from_1 = QLabel(self)
        self.lbl_from_1.setText("Время вкл. выкидного транспортера")
        self.lbl_from_1.adjustSize()
        self.lbl_from_1.move(350, 50)

        self.qle_from_1 = QLineEdit(self)
        self.qle_from_1.resize(200, 25)
        self.qle_from_1.move(350, 70)
        self.qle_from_1.setReadOnly(1)

        self.lbl_from_2 = QLabel(self)
        self.lbl_from_2.setText("Время выкл. выкидного транспортера")
        self.lbl_from_2.adjustSize()
        self.lbl_from_2.move(350, 100)

        self.qle_from_2 = QLineEdit(self)
        self.qle_from_2.resize(200, 25)
        self.qle_from_2.move(350, 120)
        self.qle_from_2.setReadOnly(1)

        self.lbl_from_3 = QLabel(self)
        self.lbl_from_3.setText("Время выкл. молотковой дробилки")
        self.lbl_from_3.adjustSize()
        self.lbl_from_3.move(350, 150)

        self.qle_from_3 = QLineEdit(self)
        self.qle_from_3.resize(200, 25)
        self.qle_from_3.move(350, 170)
        self.qle_from_3.setReadOnly(1)

        self.lbl_from_4 = QLabel(self)
        self.lbl_from_4.setText("Предпусковой звонок (вкл/выкл)")
        self.lbl_from_4.adjustSize()
        self.lbl_from_4.move(350, 200)

        self.qle_from_4 = QLineEdit(self)
        self.qle_from_4.resize(200, 25)
        self.qle_from_4.move(350, 220)
        self.qle_from_4.setReadOnly(1)

        self.lbl_from_5 = QLabel(self)
        self.lbl_from_5.setText("Время прямого вращения")
        self.lbl_from_5.adjustSize()
        self.lbl_from_5.move(350, 250)

        self.qle_from_5 = QLineEdit(self)
        self.qle_from_5.resize(200, 25)
        self.qle_from_5.move(350, 270)
        self.qle_from_5.setReadOnly(1)

        self.lbl_from_6 = QLabel(self)
        self.lbl_from_6.setText("Время обратного вращения")
        self.lbl_from_6.adjustSize()
        self.lbl_from_6.move(350, 300)

        self.qle_from_6 = QLineEdit(self)
        self.qle_from_6.resize(200, 25)
        self.qle_from_6.move(350, 320)
        self.qle_from_6.setReadOnly(1)

        self.lbl_from_7 = QLabel(self)
        self.lbl_from_7.setText("Пауза на остановку")
        self.lbl_from_7.adjustSize()
        self.lbl_from_7.move(350, 350)

        self.qle_from_7 = QLineEdit(self)
        self.qle_from_7.resize(200, 25)
        self.qle_from_7.move(350, 370)
        self.qle_from_7.setReadOnly(1)

        self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 600, 600)
        self.setWindowTitle("Настройки")


    def load_setting_data(self):
        print("load_satting")
        self.status_but = 1


    def load_kay_bord(self):
        os.startfile(r'C:\Windows\System32\osk.exe')


    def onChanged_1(self, text):
        try:
            self.time_on_conveyor_1 = float(text)
            self.qle_to_1.setStyleSheet('background : #ffffff; ')
            self.time_on_conveyor_1_status = 1
        except ValueError:
            self.qle_to_1.setStyleSheet('background : #ff4000; ')
            self.time_on_conveyor_1_status = 0
            print("ValueError")

    def onChanged_2(self, text):
        try:
            self.time_off_conveyor_1 = float(text)
            self.qle_to_2.setStyleSheet('background : #ffffff; ')
            self.time_off_conveyor_1_status = 1
        except ValueError:
            self.qle_to_2.setStyleSheet('background : #ff4000; ')
            self.time_off_conveyor_1_status = 0
            print("ValueError")

    def onChanged_3(self, text):
        try:
            self.time_off_hammer = float(text)
            self.qle_to_3.setStyleSheet('background : #ffffff; ')
            self.time_off_hammer_status = 1
        except ValueError:
            self.qle_to_3.setStyleSheet('background : #ff4000; ')
            self.time_off_hammer_status = 0
            print("ValueError")

    def onChanged_4(self, text):
        try:
            self.pre_launch = float(text)
            self.qle_to_4.setStyleSheet('background : #ffffff; ')
            self.pre_launch_status = 1
        except ValueError:
            self.qle_to_4.setStyleSheet('background : #ff4000; ')
            self.pre_launch_status = 0
            print("ValueError")

    def onChanged_5(self, text):
        try:
            self.time_direct_rotation = float(text)
            self.qle_to_5.setStyleSheet('background : #ffffff; ')
            self.time_direct_rotation_status = 1
        except ValueError:
            self.qle_to_5.setStyleSheet('background : #ff4000; ')
            self.time_direct_rotation_status = 0
            print("ValueError")

    def onChanged_6(self, text):
        try:
            self.time_reverse_rotation = float(text)
            self.qle_to_6.setStyleSheet('background : #ffffff; ')
            self.time_reverse_rotation_status = 1
        except ValueError:
            self.qle_to_6.setStyleSheet('background : #ff4000; ')
            self.time_reverse_rotation_status = 0
            print("ValueError")

    def onChanged_7(self, text):
        try:
            self.pause_to_stop = float(text)
            self.qle_to_7.setStyleSheet('background : #ffffff; ')
            self.pause_to_stop_status = 1
        except ValueError:
            self.qle_to_7.setStyleSheet('background : #ff4000; ')
            self.pause_to_stop_status = 0
            print("ValueError")






class Window_setting_hammer(QMainWindow):                           # <===
    def __init__(self):
        super().__init__()

        # <<<---
        # установка скорости мин (отключение транспортера)
        self.hammer_set_off_speed_min = 0
        # установка скорости мин (отключение транспортера)
        self.hammer_set_speed_on = 0


        self.status_but = 0


        self.qbtn_load = QPushButton('Установить настройки', self)
        self.qbtn_load.clicked.connect(self.load_setting_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_load.adjustSize()
        self.qbtn_load.move(50, 200)

        self.qbtn_2_m = QPushButton('', self)
        self.qbtn_2_m.clicked.connect(self.load_kay_bord)
        self.qbtn_2_m.setIcon(QIcon('img/kay_bord.png'))
        self.qbtn_2_m.setToolTip('Включить клавиатуру')
        self.qbtn_2_m.setIconSize(QSize(45, 45))
        self.qbtn_2_m.resize(50, 50)
        self.qbtn_2_m.move(600, 200)

        self.lbl_to = QLabel(self)
        self.lbl_to.setText("Задать настройки на ПЛК")
        self.lbl_to.adjustSize()
        self.lbl_to.move(50, 10)

        self.lbl_to_1 = QLabel(self)
        self.lbl_to_1.setText("Уставка скорости min (откл. транспортера)")
        self.lbl_to_1.adjustSize()
        self.lbl_to_1.move(50, 50)

        self.qle_to_1 = QLineEdit(self)
        self.qle_to_1.resize(200, 25)
        self.qle_to_1.move(50, 70)
        self.qle_to_1.textChanged[str].connect(self.onChanged_1)

        self.lbl_to_2 = QLabel(self)
        self.lbl_to_2.setText("Уставка скорости (вкл. транспортера)")
        self.lbl_to_2.adjustSize()
        self.lbl_to_2.move(50, 100)

        self.qle_to_2 = QLineEdit(self)
        self.qle_to_2.resize(200, 25)
        self.qle_to_2.move(50, 120)
        self.qle_to_2.textChanged[str].connect(self.onChanged_2)



        self.lbl_from = QLabel(self)
        self.lbl_from.setText("Текущие настройки на ПЛК")
        self.lbl_from.adjustSize()
        self.lbl_from.move(400, 10)

        self.lbl_from_1 = QLabel(self)
        self.lbl_from_1.setText("Уставка скорости min (откл. транспортера)")
        self.lbl_from_1.adjustSize()
        self.lbl_from_1.move(400, 50)

        self.qle_from_1 = QLineEdit(self)
        self.qle_from_1.resize(200, 25)
        self.qle_from_1.move(400, 70)
        self.qle_from_1.setReadOnly(1)

        self.lbl_from_2 = QLabel(self)
        self.lbl_from_2.setText("Уставка скорости (вкл. транспортера)")
        self.lbl_from_2.adjustSize()
        self.lbl_from_2.move(400, 100)

        self.qle_from_2 = QLineEdit(self)
        self.qle_from_2.resize(200, 25)
        self.qle_from_2.move(400, 120)
        self.qle_from_2.setReadOnly(1)


        self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 700, 300)
        self.setWindowTitle("Настройки молотковой дробилки")



    def load_setting_data(self):
        print("load_satting_hammer")
        self.status_but = 1


    def load_kay_bord(self):
        os.startfile(r'C:\Windows\System32\osk.exe')


    def onChanged_1(self, text):
        try:
            self.hammer_set_off_speed_min = float(text)
            self.qle_to_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_2(self, text):
        try:
            self.hammer_set_speed_on = float(text)
            self.qle_to_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")


class Window_tooth_setting(QMainWindow):                           # <===
    def __init__(self):
        super().__init__()

        # <<<---
        # установка отключения по скорости min
        self.val_set_off_speed_min_1 = 0
        self.val_set_off_speed_min_2 = 0
        self.val_set_off_speed_min_3 = 0
        self.val_set_off_speed_min_4 = 0
        self.val_set_off_speed_min_5 = 0
        self.val_set_off_speed_min_6 = 0

        # установка времени включения
        self.val_set_on_time_1 = 0
        self.val_set_on_time_2 = 0
        self.val_set_on_time_3 = 0
        self.val_set_on_time_4 = 0
        self.val_set_on_time_5 = 0
        self.val_set_on_time_6 = 0

        # установка времени фильтрации(срабатывание)
        self.val_set_time_actuation_1 = 0
        self.val_set_time_actuation_2 = 0
        self.val_set_time_actuation_3 = 0
        self.val_set_time_actuation_4 = 0
        self.val_set_time_actuation_5 = 0
        self.val_set_time_actuation_6 = 0

        # установка времени на отсутствие импульсов
        self.val_set_time_pulse_off_1 = 0
        self.val_set_time_pulse_off_2 = 0
        self.val_set_time_pulse_off_3 = 0
        self.val_set_time_pulse_off_4 = 0
        self.val_set_time_pulse_off_5 = 0
        self.val_set_time_pulse_off_6 = 0

        self.status_but = 0


        self.qbtn_load = QPushButton('Установить настройки', self)
        self.qbtn_load.clicked.connect(self.load_setting_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_load.adjustSize()
        self.qbtn_load.move(1050, 230)

        self.qbtn_2_m = QPushButton('', self)
        self.qbtn_2_m.clicked.connect(self.load_kay_bord)
        self.qbtn_2_m.setIcon(QIcon('img/kay_bord.png'))
        self.qbtn_2_m.setToolTip('Включить клавиатуру')
        self.qbtn_2_m.setIconSize(QSize(45, 45))
        self.qbtn_2_m.resize(50, 50)
        self.qbtn_2_m.move(1180, 500)

        self.lbl_to = QLabel(self)
        self.lbl_to.setText("Задать настройки на ПЛК")
        self.lbl_to.adjustSize()
        self.lbl_to.move(50, 50)

        self.lbl_to_1 = QLabel(self)
        self.lbl_to_1.setText("Установка по скорости min")
        self.lbl_to_1.adjustSize()
        self.lbl_to_1.move(0, 100)

        self.lbl_to_2 = QLabel(self)
        self.lbl_to_2.setText("Установка времени включения")
        self.lbl_to_2.adjustSize()
        self.lbl_to_2.move(0, 130)

        self.lbl_to_3 = QLabel(self)
        self.lbl_to_3.setText("Установка времени фильтрации (срабатывания)")
        self.lbl_to_3.adjustSize()
        self.lbl_to_3.move(0, 160)

        self.lbl_to_4 = QLabel(self)
        self.lbl_to_4.setText("Установка времени на отсутствие импульсов")
        self.lbl_to_4.adjustSize()
        self.lbl_to_4.move(0, 190)


        self.lbl_to_m_1 = QLabel(self)
        self.lbl_to_m_1.setText("Вал №1")
        self.lbl_to_m_1.adjustSize()
        self.lbl_to_m_1.move(300, 80)

        self.qle_to_m_1_p_1 = QLineEdit(self)
        self.qle_to_m_1_p_1.resize(100, 25)
        self.qle_to_m_1_p_1.move(300, 100)
        self.qle_to_m_1_p_1.textChanged[str].connect(self.onChanged_1_p_1)

        self.qle_to_m_1_p_2 = QLineEdit(self)
        self.qle_to_m_1_p_2.resize(100, 25)
        self.qle_to_m_1_p_2.move(300, 130)
        self.qle_to_m_1_p_2.textChanged[str].connect(self.onChanged_1_p_2)

        self.qle_to_m_1_p_3 = QLineEdit(self)
        self.qle_to_m_1_p_3.resize(100, 25)
        self.qle_to_m_1_p_3.move(300, 160)
        self.qle_to_m_1_p_3.textChanged[str].connect(self.onChanged_1_p_3)

        self.qle_to_m_1_p_4 = QLineEdit(self)
        self.qle_to_m_1_p_4.resize(100, 25)
        self.qle_to_m_1_p_4.move(300, 190)
        self.qle_to_m_1_p_4.textChanged[str].connect(self.onChanged_1_p_4)

        self.lbl_to_m_2 = QLabel(self)
        self.lbl_to_m_2.setText("Вал №2")
        self.lbl_to_m_2.adjustSize()
        self.lbl_to_m_2.move(450, 80)

        self.qle_to_m_2_p_1 = QLineEdit(self)
        self.qle_to_m_2_p_1.resize(100, 25)
        self.qle_to_m_2_p_1.move(450, 100)
        self.qle_to_m_2_p_1.textChanged[str].connect(self.onChanged_2_p_1)

        self.qle_to_m_2_p_2 = QLineEdit(self)
        self.qle_to_m_2_p_2.resize(100, 25)
        self.qle_to_m_2_p_2.move(450, 130)
        self.qle_to_m_2_p_2.textChanged[str].connect(self.onChanged_2_p_2)

        self.qle_to_m_2_p_3 = QLineEdit(self)
        self.qle_to_m_2_p_3.resize(100, 25)
        self.qle_to_m_2_p_3.move(450, 160)
        self.qle_to_m_2_p_3.textChanged[str].connect(self.onChanged_2_p_3)

        self.qle_to_m_2_p_4 = QLineEdit(self)
        self.qle_to_m_2_p_4.resize(100, 25)
        self.qle_to_m_2_p_4.move(450, 190)
        self.qle_to_m_2_p_4.textChanged[str].connect(self.onChanged_2_p_4)

        self.lbl_to_m_3 = QLabel(self)
        self.lbl_to_m_3.setText("Вал №3")
        self.lbl_to_m_3.adjustSize()
        self.lbl_to_m_3.move(600, 80)

        self.qle_to_m_3_p_1 = QLineEdit(self)
        self.qle_to_m_3_p_1.resize(100, 25)
        self.qle_to_m_3_p_1.move(600, 100)
        self.qle_to_m_3_p_1.textChanged[str].connect(self.onChanged_3_p_1)

        self.qle_to_m_3_p_2 = QLineEdit(self)
        self.qle_to_m_3_p_2.resize(100, 25)
        self.qle_to_m_3_p_2.move(600, 130)
        self.qle_to_m_3_p_2.textChanged[str].connect(self.onChanged_3_p_2)

        self.qle_to_m_3_p_3 = QLineEdit(self)
        self.qle_to_m_3_p_3.resize(100, 25)
        self.qle_to_m_3_p_3.move(600, 160)
        self.qle_to_m_3_p_3.textChanged[str].connect(self.onChanged_3_p_3)

        self.qle_to_m_3_p_4 = QLineEdit(self)
        self.qle_to_m_3_p_4.resize(100, 25)
        self.qle_to_m_3_p_4.move(600, 190)
        self.qle_to_m_3_p_4.textChanged[str].connect(self.onChanged_3_p_4)

        self.lbl_to_m_4 = QLabel(self)
        self.lbl_to_m_4.setText("Вал №4")
        self.lbl_to_m_4.adjustSize()
        self.lbl_to_m_4.move(750, 80)

        self.qle_to_m_4_p_1 = QLineEdit(self)
        self.qle_to_m_4_p_1.resize(100, 25)
        self.qle_to_m_4_p_1.move(750, 100)
        self.qle_to_m_4_p_1.textChanged[str].connect(self.onChanged_4_p_1)

        self.qle_to_m_4_p_2 = QLineEdit(self)
        self.qle_to_m_4_p_2.resize(100, 25)
        self.qle_to_m_4_p_2.move(750, 130)
        self.qle_to_m_4_p_2.textChanged[str].connect(self.onChanged_4_p_2)

        self.qle_to_m_4_p_3 = QLineEdit(self)
        self.qle_to_m_4_p_3.resize(100, 25)
        self.qle_to_m_4_p_3.move(750, 160)
        self.qle_to_m_4_p_3.textChanged[str].connect(self.onChanged_4_p_3)

        self.qle_to_m_4_p_4 = QLineEdit(self)
        self.qle_to_m_4_p_4.resize(100, 25)
        self.qle_to_m_4_p_4.move(750, 190)
        self.qle_to_m_4_p_4.textChanged[str].connect(self.onChanged_4_p_4)


        self.lbl_to_m_5 = QLabel(self)
        self.lbl_to_m_5.setText("Вал №5")
        self.lbl_to_m_5.adjustSize()
        self.lbl_to_m_5.move(900, 80)

        self.qle_to_m_5_p_1 = QLineEdit(self)
        self.qle_to_m_5_p_1.resize(100, 25)
        self.qle_to_m_5_p_1.move(900, 100)
        self.qle_to_m_5_p_1.textChanged[str].connect(self.onChanged_5_p_1)

        self.qle_to_m_5_p_2 = QLineEdit(self)
        self.qle_to_m_5_p_2.resize(100, 25)
        self.qle_to_m_5_p_2.move(900, 130)
        self.qle_to_m_5_p_2.textChanged[str].connect(self.onChanged_5_p_2)

        self.qle_to_m_5_p_3 = QLineEdit(self)
        self.qle_to_m_5_p_3.resize(100, 25)
        self.qle_to_m_5_p_3.move(900, 160)
        self.qle_to_m_5_p_3.textChanged[str].connect(self.onChanged_5_p_3)

        self.qle_to_m_5_p_4 = QLineEdit(self)
        self.qle_to_m_5_p_4.resize(100, 25)
        self.qle_to_m_5_p_4.move(900, 190)
        self.qle_to_m_5_p_4.textChanged[str].connect(self.onChanged_5_p_4)

        self.lbl_to_m_6 = QLabel(self)
        self.lbl_to_m_6.setText("Вал №6")
        self.lbl_to_m_6.adjustSize()
        self.lbl_to_m_6.move(1050, 80)

        self.qle_to_m_6_p_1 = QLineEdit(self)
        self.qle_to_m_6_p_1.resize(100, 25)
        self.qle_to_m_6_p_1.move(1050, 100)
        self.qle_to_m_6_p_1.textChanged[str].connect(self.onChanged_6_p_1)

        self.qle_to_m_6_p_2 = QLineEdit(self)
        self.qle_to_m_6_p_2.resize(100, 25)
        self.qle_to_m_6_p_2.move(1050, 130)
        self.qle_to_m_6_p_2.textChanged[str].connect(self.onChanged_6_p_2)

        self.qle_to_m_6_p_3 = QLineEdit(self)
        self.qle_to_m_6_p_3.resize(100, 25)
        self.qle_to_m_6_p_3.move(1050, 160)
        self.qle_to_m_6_p_3.textChanged[str].connect(self.onChanged_6_p_3)

        self.qle_to_m_6_p_4 = QLineEdit(self)
        self.qle_to_m_6_p_4.resize(100, 25)
        self.qle_to_m_6_p_4.move(1050, 190)
        self.qle_to_m_6_p_4.textChanged[str].connect(self.onChanged_6_p_4)


        y_from = 250

        self.lbl_from = QLabel(self)
        self.lbl_from.setText("Текущие настрйки на ПЛК")
        self.lbl_from.adjustSize()
        self.lbl_from.move(50, y_from+50)

        self.lbl_from_1 = QLabel(self)
        self.lbl_from_1.setText("Установка по скорости min")
        self.lbl_from_1.adjustSize()
        self.lbl_from_1.move(0, y_from+100)

        self.lbl_from_2 = QLabel(self)
        self.lbl_from_2.setText("Установка времени включения")
        self.lbl_from_2.adjustSize()
        self.lbl_from_2.move(0, y_from+130)

        self.lbl_from_3 = QLabel(self)
        self.lbl_from_3.setText("Установка времени фильтрации (срабатывания)")
        self.lbl_from_3.adjustSize()
        self.lbl_from_3.move(0, y_from+160)

        self.lbl_from_4 = QLabel(self)
        self.lbl_from_4.setText("Установка времени на отсутствие импульсов")
        self.lbl_from_4.adjustSize()
        self.lbl_from_4.move(0, y_from+190)


        self.lbl_from_m_1 = QLabel(self)
        self.lbl_from_m_1.setText("Вал №1")
        self.lbl_from_m_1.adjustSize()
        self.lbl_from_m_1.move(300, y_from+80)

        self.qle_from_m_1_p_1 = QLineEdit(self)
        self.qle_from_m_1_p_1.resize(100, 25)
        self.qle_from_m_1_p_1.move(300, y_from+100)
        self.qle_from_m_1_p_1.setReadOnly(1)

        self.qle_from_m_1_p_2 = QLineEdit(self)
        self.qle_from_m_1_p_2.resize(100, 25)
        self.qle_from_m_1_p_2.move(300, y_from+130)
        self.qle_from_m_1_p_2.setReadOnly(1)

        self.qle_from_m_1_p_3 = QLineEdit(self)
        self.qle_from_m_1_p_3.resize(100, 25)
        self.qle_from_m_1_p_3.move(300, y_from+160)
        self.qle_from_m_1_p_3.setReadOnly(1)

        self.qle_from_m_1_p_4 = QLineEdit(self)
        self.qle_from_m_1_p_4.resize(100, 25)
        self.qle_from_m_1_p_4.move(300, y_from+190)
        self.qle_from_m_1_p_4.setReadOnly(1)

        self.lbl_from_m_2 = QLabel(self)
        self.lbl_from_m_2.setText("Вал №2")
        self.lbl_from_m_2.adjustSize()
        self.lbl_from_m_2.move(450, y_from+80)

        self.qle_from_m_2_p_1 = QLineEdit(self)
        self.qle_from_m_2_p_1.resize(100, 25)
        self.qle_from_m_2_p_1.move(450, y_from+100)
        self.qle_from_m_2_p_1.setReadOnly(1)

        self.qle_from_m_2_p_2 = QLineEdit(self)
        self.qle_from_m_2_p_2.resize(100, 25)
        self.qle_from_m_2_p_2.move(450, y_from+130)
        self.qle_from_m_2_p_2.setReadOnly(1)

        self.qle_from_m_2_p_3 = QLineEdit(self)
        self.qle_from_m_2_p_3.resize(100, 25)
        self.qle_from_m_2_p_3.move(450, y_from+160)
        self.qle_from_m_2_p_3.setReadOnly(1)

        self.qle_from_m_2_p_4 = QLineEdit(self)
        self.qle_from_m_2_p_4.resize(100, 25)
        self.qle_from_m_2_p_4.move(450, y_from+190)
        self.qle_from_m_2_p_4.setReadOnly(1)

        self.lbl_from_m_3 = QLabel(self)
        self.lbl_from_m_3.setText("Вал №3")
        self.lbl_from_m_3.adjustSize()
        self.lbl_from_m_3.move(600, y_from+80)

        self.qle_from_m_3_p_1 = QLineEdit(self)
        self.qle_from_m_3_p_1.resize(100, 25)
        self.qle_from_m_3_p_1.move(600, y_from+100)
        self.qle_from_m_3_p_1.setReadOnly(1)

        self.qle_from_m_3_p_2 = QLineEdit(self)
        self.qle_from_m_3_p_2.resize(100, 25)
        self.qle_from_m_3_p_2.move(600, y_from+130)
        self.qle_from_m_3_p_2.setReadOnly(1)

        self.qle_from_m_3_p_3 = QLineEdit(self)
        self.qle_from_m_3_p_3.resize(100, 25)
        self.qle_from_m_3_p_3.move(600, y_from+160)
        self.qle_from_m_3_p_3.setReadOnly(1)

        self.qle_from_m_3_p_4 = QLineEdit(self)
        self.qle_from_m_3_p_4.resize(100, 25)
        self.qle_from_m_3_p_4.move(600, y_from+190)
        self.qle_from_m_3_p_4.setReadOnly(1)

        self.lbl_from_m_4 = QLabel(self)
        self.lbl_from_m_4.setText("Вал №4")
        self.lbl_from_m_4.adjustSize()
        self.lbl_from_m_4.move(750, y_from+80)

        self.qle_from_m_4_p_1 = QLineEdit(self)
        self.qle_from_m_4_p_1.resize(100, 25)
        self.qle_from_m_4_p_1.move(750, y_from+100)
        self.qle_from_m_4_p_1.setReadOnly(1)

        self.qle_from_m_4_p_2 = QLineEdit(self)
        self.qle_from_m_4_p_2.resize(100, 25)
        self.qle_from_m_4_p_2.move(750, y_from+130)
        self.qle_from_m_4_p_2.setReadOnly(1)

        self.qle_from_m_4_p_3 = QLineEdit(self)
        self.qle_from_m_4_p_3.resize(100, 25)
        self.qle_from_m_4_p_3.move(750, y_from+160)
        self.qle_from_m_4_p_3.setReadOnly(1)

        self.qle_from_m_4_p_4 = QLineEdit(self)
        self.qle_from_m_4_p_4.resize(100, 25)
        self.qle_from_m_4_p_4.move(750, y_from+190)
        self.qle_from_m_4_p_4.setReadOnly(1)

        self.lbl_from_m_5 = QLabel(self)
        self.lbl_from_m_5.setText("Вал №5")
        self.lbl_from_m_5.adjustSize()
        self.lbl_from_m_5.move(900, y_from+80)

        self.qle_from_m_5_p_1 = QLineEdit(self)
        self.qle_from_m_5_p_1.resize(100, 25)
        self.qle_from_m_5_p_1.move(900, y_from+100)
        self.qle_from_m_5_p_1.setReadOnly(1)

        self.qle_from_m_5_p_2 = QLineEdit(self)
        self.qle_from_m_5_p_2.resize(100, 25)
        self.qle_from_m_5_p_2.move(900, y_from+130)
        self.qle_from_m_5_p_2.setReadOnly(1)

        self.qle_from_m_5_p_3 = QLineEdit(self)
        self.qle_from_m_5_p_3.resize(100, 25)
        self.qle_from_m_5_p_3.move(900, y_from+160)
        self.qle_from_m_5_p_3.setReadOnly(1)

        self.qle_from_m_5_p_4 = QLineEdit(self)
        self.qle_from_m_5_p_4.resize(100, 25)
        self.qle_from_m_5_p_4.move(900, y_from+190)
        self.qle_from_m_5_p_4.setReadOnly(1)

        self.lbl_from_m_6 = QLabel(self)
        self.lbl_from_m_6.setText("Вал №6")
        self.lbl_from_m_6.adjustSize()
        self.lbl_from_m_6.move(1050, y_from+80)

        self.qle_from_m_6_p_1 = QLineEdit(self)
        self.qle_from_m_6_p_1.resize(100, 25)
        self.qle_from_m_6_p_1.move(1050, y_from+100)
        self.qle_from_m_6_p_1.setReadOnly(1)

        self.qle_from_m_6_p_2 = QLineEdit(self)
        self.qle_from_m_6_p_2.resize(100, 25)
        self.qle_from_m_6_p_2.move(1050, y_from+130)
        self.qle_from_m_6_p_2.setReadOnly(1)

        self.qle_from_m_6_p_3 = QLineEdit(self)
        self.qle_from_m_6_p_3.resize(100, 25)
        self.qle_from_m_6_p_3.move(1050, y_from+160)
        self.qle_from_m_6_p_3.setReadOnly(1)

        self.qle_from_m_6_p_4 = QLineEdit(self)
        self.qle_from_m_6_p_4.resize(100, 25)
        self.qle_from_m_6_p_4.move(1050, y_from+190)
        self.qle_from_m_6_p_4.setReadOnly(1)

        self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 1280, 600)
        self.setWindowTitle("Настройки разрывной дробилки")


    def load_setting_data(self):
        print("load_satting")
        self.status_but = 1

    def load_kay_bord(self):
        os.startfile(r'C:\Windows\System32\osk.exe')


    def onChanged_1_p_1(self, text):
        try:
            self.val_set_off_speed_min_1 = float(text)
            self.qle_to_m_1_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_1_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_1_p_2(self, text):
        try:
            self.val_set_on_time_1 = float(text)
            self.qle_to_m_1_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_1_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_1_p_3(self, text):
        try:
            self.val_set_time_actuation_1 = float(text)
            self.qle_to_m_1_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_1_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_1_p_4(self, text):
        try:
            self.val_set_time_pulse_off_1 = float(text)
            self.qle_to_m_1_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_1_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")



    def onChanged_2_p_1(self, text):
        try:
            self.val_set_off_speed_min_2 = float(text)
            self.qle_to_m_2_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_2_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_2_p_2(self, text):
        try:
            self.val_set_on_time_2 = float(text)
            self.qle_to_m_2_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_2_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_2_p_3(self, text):
        try:
            self.val_set_time_actuation_2 = float(text)
            self.qle_to_m_2_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_2_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_2_p_4(self, text):
        try:
            self.val_set_time_pulse_off_2 = float(text)
            self.qle_to_m_2_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_2_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")


    def onChanged_3_p_1(self, text):
        try:
            self.val_set_off_speed_min_3 = float(text)
            self.qle_to_m_3_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_3_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_3_p_2(self, text):
        try:
            self.val_set_on_time_3 = float(text)
            self.qle_to_m_3_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_3_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_3_p_3(self, text):
        try:
            self.val_set_time_actuation_3 = float(text)
            self.qle_to_m_3_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_3_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_3_p_4(self, text):
        try:
            self.val_set_time_pulse_off_3 = float(text)
            self.qle_to_m_3_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_3_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")


    def onChanged_4_p_1(self, text):
        try:
            self.val_set_off_speed_min_4 = float(text)
            self.qle_to_m_4_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_4_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_4_p_2(self, text):
        try:
            self.val_set_on_time_4 = float(text)
            self.qle_to_m_4_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_4_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_4_p_3(self, text):
        try:
            self.val_set_time_actuation_4 = float(text)
            self.qle_to_m_4_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_4_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_4_p_4(self, text):
        try:
            self.val_set_time_pulse_off_4 = float(text)
            self.qle_to_m_4_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_4_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")


    def onChanged_5_p_1(self, text):
        try:
            self.val_set_off_speed_min_5 = float(text)
            self.qle_to_m_5_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_5_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_5_p_2(self, text):
        try:
            self.val_set_on_time_5 = float(text)
            self.qle_to_m_5_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_5_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_5_p_3(self, text):
        try:
            self.val_set_time_actuation_5 = float(text)
            self.qle_to_m_5_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_5_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_5_p_4(self, text):
        try:
            self.val_set_time_pulse_off_5 = float(text)
            self.qle_to_m_5_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_5_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")


    def onChanged_6_p_1(self, text):
        try:
            self.val_set_off_speed_min_6 =float(text)
            self.qle_to_m_6_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_6_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_6_p_2(self, text):
        try:
            self.val_set_on_time_6 =float(text)
            self.qle_to_m_6_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_6_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_6_p_3(self, text):
        try:
            self.val_set_time_actuation_6 =float(text)
            self.qle_to_m_6_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_6_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_6_p_4(self, text):
        try:
            self.val_set_time_pulse_off_6 =float(text)
            self.qle_to_m_6_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_6_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")
